import {takeLatest} from "redux-saga/effects";
import store from "../../store/store";
import Axios from "axios";
import {STORE_DISTRICT_DATA, STORE_STATE_DATA} from "./action";

export const watchDataFetch = function* () {
    yield takeLatest("DATA_FETCH", workerDataFetch);
};

function* workerDataFetch(){
    yield store.dispatch({type:"LOADING",payload: {loading:true}})
    yield Axios.get("https://api.covid19india.org/data.json").then(function(response){
        store.dispatch({type:STORE_STATE_DATA , payload:response.data})
        store.dispatch({type:"LOADING",payload: {loading:false}})
        console.log(response.data)
    })
    yield Axios.get("https://api.covid19india.org/state_district_wise.json").then(function(response){
        console.log(response.data)
        store.dispatch({type:STORE_DISTRICT_DATA, payload:response.data})
    })

}

