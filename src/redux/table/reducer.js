const initialState={
    data:[{statewise:[]}],
    loading:false,
    tableHead:[],
    districtData:[]
}

export const tableReducer = (state = initialState, action) => {
        switch (action.type) {
            case "STORE_STATE_DATA":
                state.tableHead = action.payload.statewise[0];
                action.payload.statewise.splice(0,1);
                return {...state, data:action.payload};
            case "LOADING":
                return {...state, ...action.payload};
            case "STORE_DISTRICT_DATA":
                return {...state, districtData:action.payload};
            default:
                return state;
        }

}
