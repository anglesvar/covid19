import {combineReducers} from 'redux';
import {tableReducer} from '../redux/table/reducer'

const reducer= combineReducers({tableReducer})

export default reducer;

