import {all} from 'redux-saga/effects';
import {watchDataFetch} from '../redux/table/sagas'

export default function* rootSaga() {
    yield all([watchDataFetch()])
}