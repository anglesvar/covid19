import React from "react";
import {connect} from "react-redux";
import store from "../../store/store";
import {Table} from "antd";
import './index.css'
import moment from "moment";
import {DATA_FETCH} from "../../redux/table/action";


class TableComponent extends React.Component{
    componentDidMount() {
        store.dispatch({type:DATA_FETCH,payload:1})
    }
    render() {
        const columns = [
            {
                title: 'State',
                dataIndex: 'state',
                key: 'state',
                render: text => (<div style={{fontWeight:900}} onClick={()=>expandedRowRender(text)}>{text}</div>),

            },
            {
                title: 'Confirmed Cases',
                dataIndex: 'confirmed',
                defaultSortOrder: 'descend',key: 'confirmed',
                sorter: (a, b) => a.confirmed - b.confirmed,
            },
            {
                title: <div style={{color:'blue',fontWeight:700}}>Active Cases</div>,
                dataIndex: 'active',
                key: 'active'
            },
            {

                title: <div style={{color:'red',fontWeight:700}}>Deaths</div>,
                dataIndex: 'deaths',
                key: 'deaths'
            },
            {
                title: <div style={{color:'green',fontWeight:700}}>Recovered</div>,
                dataIndex: 'recovered',
                key: 'recovered'
            },
            {
                title: 'Last Updated Time',
                dataIndex: 'lastupdatedtime',
                key: 'lastupdatedtime',
                render: text => (moment(text,"mm-dd-yyyy hh:mm:ss").format("DD-MMM-YYYY HH:MM:SS")),
            }
        ];

        const expandedRowRender = (state) => {
            const columns = [
                { title: 'Date', dataIndex: 'lastupdatedtime',key: 'lastupdatedtime' },
                { title: 'Name', dataIndex: 'confirmed' ,key: 'confirmed'},

            ];

            let {districtData} = this.props.tableReducer.districtData[state];

            console.log(districtData);
            console.log(<Table columns={columns} dataSource={districtData} pagination={false} />);
            return <Table columns={columns} dataSource={districtData} pagination={false} />;
        };

        let {statewise} = this.props.tableReducer.data;
        return (<div>
            <Table columns={columns} className="components-table-demo-nested" expandable={expandedRowRender} scroll={{x:true}} rowKey={e=>e.statecode} dataSource={statewise} pagination={{ pageSize: 50 }} bordered={true} />
                <h4 style={{textAlign: 'center'}}>Designed by <bold style={{color:'red'}}>Anglesvar Chandrasekar Appiya<span role={"img"}>♥</span>️</bold></h4>
        </div>
        )
    }
}

const mapStateToProps = state => {
    return {...state};
};
export default connect(mapStateToProps)(TableComponent);
