import React from "react";
import {connect} from "react-redux";
import { Row, Col, Alert } from 'antd';
import "./index.css";

class HeaderComponent extends React.Component{
    render() {
        const {active,confirmed,deaths} = this.props.tableReducer.tableHead
        return(<div>
                <h1 style={{textAlign:'center',fontWeight:800,fontSize:35}}>INDIA - COVID19 LIVE UPDATE</h1>
                <Alert
                    message="In this time of Global Emergency, the immediate actions taken by us will impact the rapidity of the spread of COVID-19, Be INFORMED!! Be SAFE!! Stay at Home!!"
                    banner
                    closable
                    showIcon={false}
                />
                <br />
                <Row>
                <Col lg={{span: 4, offset: 4}} sm={{span: 4, offset: 4}} md={{span: 4, offset: 4}} xs={{span:4,offset:2}}><div className={"covid-active"} >Active<h1>{active}</h1></div></Col>
                <Col lg={{span:4,offset:3}} sm={{span:4,offset:4}} md={{span:4,offset:4}} xs={{span:4,offset:4}}><div className={"covid-confirm"} >Confirmed<h1>{confirmed}</h1></div></Col>
                <Col lg={{span:4, offset:3}} sm={{span:4,offset:4}} md={{span:4,offset:4}} xs={{span:4,offset:4}}><div className={"covid-deaths"} >Deaths<h1>{deaths}</h1></div></Col>
            </Row>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {...state};
};
export default connect(mapStateToProps)(HeaderComponent);
