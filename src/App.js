import React from 'react';
import './App.css';
import TableComponent from "./components/table";
import store from "./store/store";
import {Provider} from "react-redux";
import "antd/dist/antd.css";
import HeaderComponent from "./components/Header";


class App extends React.Component{
    render() {
        return (
            <Provider store={store}>
                <HeaderComponent />
                <TableComponent />
            </Provider>
        );
    }
}

export default App;
